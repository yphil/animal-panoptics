# Animal Panoptics

A full, music-and-graphics album made with [Sunvox](http://www.warmplace.ru/soft/sunvox/) (music) and [Pixilang](http://www.warmplace.ru/soft/pixilang/) (visual code) ; If you're into that stuff, see also [pixilang-mode](https://bitbucket.org/yassinphilip/pixilang-mode).

![screenshot](https://bitbucket.org/yassinphilip/animal-panoptics/raw/master/screenshot.png)

## Usage

Run [PLAY.sh](https://bitbucket.org/yassinphilip/animal-panoptics/src/master/PLAY.sh) ; Maximize window ; Turn volume up.

### Controls

* LEFT or [      - previous track
* RIGHT or ]     - next track
* UP or +        - volume up
* DOWN or -      - volume down
* ENTER          - rewind
* SPACE          - pause
* ESCAPE         - exit
* TAP THE SCREEN - show this text

Made around late 2012 ; Enjoy, but don't overdo it, please.