#!/usr/bin/env bash
p="animal_panoptics.pixi"

if [[ "$OSTYPE" == "linux-gnu" ]]; then

    if [ $(uname -m) == 'x86_64' ]; then
        ./pixilang/linux_x86_64/pixilang ${p}
    else
        ./pixilang/linux_x86/pixilang ${p}
    fi
else
    pixilang/windows/pixilang.exe ${p}
fi
